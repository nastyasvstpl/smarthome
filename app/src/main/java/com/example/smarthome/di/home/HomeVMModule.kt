package com.example.smarthome.di.home

import androidx.lifecycle.ViewModel
import com.example.smarthome.di.VMKey
import com.example.smarthome.screens.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class HomeVMModule {
    @Binds
    @IntoMap
    @VMKey(HomeViewModel::class)
    abstract fun postListViewModel(viewModel: HomeViewModel): ViewModel

}