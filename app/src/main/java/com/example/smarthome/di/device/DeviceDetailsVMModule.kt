package com.example.smarthome.di.device

import androidx.lifecycle.ViewModel
import com.example.smarthome.di.VMKey
import com.example.smarthome.screens.device.DeviceDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DeviceDetailsVMModule {
    @Binds
    @IntoMap
    @VMKey(DeviceDetailsViewModel::class)
    abstract fun postListViewModel(viewModel: DeviceDetailsViewModel): ViewModel

}