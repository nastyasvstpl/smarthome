package com.example.smarthome.di

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class ViewModelFactoryModule {
    @Singleton
    @Binds
    abstract fun bindVMFactory(viewModelProviderFactory: ViewModelProviderFactory): ViewModelProvider.Factory
}
