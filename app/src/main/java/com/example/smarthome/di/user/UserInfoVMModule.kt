package com.example.smarthome.di.user

import androidx.lifecycle.ViewModel
import com.example.smarthome.di.VMKey
import com.example.smarthome.screens.user.UserInfoViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class UserInfoVMModule {
    @Binds
    @IntoMap
    @VMKey(UserInfoViewModel::class)
    abstract fun postListViewModel(viewModel: UserInfoViewModel): ViewModel

}