package com.example.smarthome.di

import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.smarthome.R
import com.example.smarthome.screens.main.MainActivity
import dagger.Module
import dagger.Provides

@Module
class MainModule {
    @Provides
    fun provideNavigationController(mainActivity: MainActivity): NavController {
        return Navigation.findNavController(mainActivity, R.id.nav_host_fragment)
    }
}
