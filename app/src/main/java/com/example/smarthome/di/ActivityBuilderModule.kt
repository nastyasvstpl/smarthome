package com.example.smarthome.di

import com.example.smarthome.screens.main.MainActivity
import com.example.smarthome.screens.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(
        modules = arrayOf(
            FragmentBuildersModule::class,
            MainModule::class
        )
    )
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity
}
