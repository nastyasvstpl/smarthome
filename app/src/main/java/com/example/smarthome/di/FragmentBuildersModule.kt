package com.example.smarthome.di

import com.example.smarthome.di.device.DeviceDetailsVMModule
import com.example.smarthome.di.home.HomeVMModule
import com.example.smarthome.di.user.UserInfoVMModule
import com.example.smarthome.screens.device.DeviceDetailsFragment
import com.example.smarthome.screens.home.HomeFragment
import com.example.smarthome.screens.user.UserInfoFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector(modules = [HomeVMModule::class])
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector(modules = [DeviceDetailsVMModule::class])
    abstract fun contributeDeviceDetailsFragment(): DeviceDetailsFragment

    @ContributesAndroidInjector(modules = [UserInfoVMModule::class])
    abstract fun contributeUserInfoFragment(): UserInfoFragment

}
