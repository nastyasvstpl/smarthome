package com.example.smarthome.screens.home.adapters

import android.view.ViewGroup
import com.example.smarthome.R
import com.example.smarthome.base.BaseBindingAdapter
import com.example.smarthome.databinding.ItemFilterBinding
import com.example.smarthome.models.Filter

class FilterAdapter : BaseBindingAdapter<Filter, FilterAdapter.ViewHolder, ItemFilterBinding>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(initBinding(R.layout.item_filter, parent))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        if (isInPositionsRange(position)) {
            holder.binding.checkbox.setOnCheckedChangeListener { compoundButton, isChecked ->
                items[holder.adapterPosition].isSelected = isChecked
            }
            holder.onBind(items[holder.adapterPosition], holder.adapterPosition)
        }
    }

    class ViewHolder(binding: ItemFilterBinding) : BaseBindingViewHolder<Filter, ItemFilterBinding>(binding) {
        override fun onBind(item: Filter, position: Int) {
            binding.checkbox.text = item.title
            binding.checkbox.isChecked = item.isSelected
        }
    }

}




