package com.example.smarthome.screens.user

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.example.smarthome.R
import com.example.smarthome.base.BaseFragment
import com.example.smarthome.databinding.FragmentUserInfoBinding
import com.example.smarthome.di.ViewModelProviderFactory
import com.example.smarthome.models.Address
import com.example.smarthome.models.User
import kotlinx.android.synthetic.main.fragment_user_info.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class UserInfoFragment : BaseFragment<FragmentUserInfoBinding>() {

    override val layoutId = R.layout.fragment_user_info

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory
    private lateinit var viewModel: UserInfoViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(
            requireActivity(),
            viewModelProviderFactory
        ).get(UserInfoViewModel::class.java)
        viewModel.requireUser()
        setupObservables()
        setupListeners()
    }

    private fun setupListeners() {
        binding.saveBtn.setOnClickListener {
            if (isValid()){
                viewModel.saveUserToPref(updateUser())
                Toast.makeText(requireContext(),"Settings successfully changed",Toast.LENGTH_SHORT).show()
            }
        }
        binding.birth.setOnClickListener {
            binding.datePicker.isVisible = !binding.datePicker.isVisible
            binding.getDate.isVisible = !binding.getDate.isVisible
        }
        binding.getDate.setOnClickListener {
            binding.birth.text = StringBuilder()
                    .append(binding.datePicker.dayOfMonth).append(".")
                    .append(binding.datePicker.month + 1).append(".")
                    .append(binding.datePicker.year)
            binding.datePicker.isVisible = false
            binding.getDate.isVisible = false
        }

    }

    private fun setupObservables() {
        viewModel.userLiveData.observe(viewLifecycleOwner) { user ->
            if (user == null) return@observe
            with(binding) {
                firstNameEt.setText(user.firstName)
                lastNameEt.setText(user.lastName)
                cityEt.setText(user.address?.city)
                postCodeEt.setText(user.address?.postalCode.toString())
                streetEt.setText(user.address?.street)
                streetCodeEt.setText(user.address?.streetCode)
                countryEt.setText(user.address?.country)
                birth.setText(user.birthDate?.let { convertLongToDate(it) })
            }
        }
    }

    private fun convertLongToDate(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("dd.MM.yyyy")
        return format.format(date)
    }

    private fun convertDateToLong(date: String): Long {
        val dateFormat = SimpleDateFormat("dd.MM.yyyy")
        return dateFormat.parse(date).time
    }

    private fun updateUser(): User {
        return User(
            address = Address(
                city = binding.cityEt.text.toString(),
                postalCode = binding.postCodeEt.text.toString().toInt(),
                street = binding.streetEt.text.toString(),
                streetCode = binding.streetCodeEt.text.toString(),
                country = binding.countryEt.text.toString()
            ),
            firstName = binding.firstNameEt.text.toString(),
            lastName = binding.lastNameEt.text.toString(),
            birthDate = binding.birth.text.let { convertDateToLong(it.toString()) }
        )
    }

    private fun isValid(): Boolean {
        with(binding) {
            val views = sequenceOf(
                cityEt,
                postCodeEt,
                streetEt,
                streetCodeEt,
                countryEt,
                firstNameEt,
                lastNameEt
            )
            val isEmptyFields = views.any { view -> view.text.isEmpty() || view.text.isBlank() }
            if (isEmptyFields) {
                Toast.makeText(requireContext(), "Fields should not be empty", Toast.LENGTH_LONG)
                    .show()
                return false
            }
            return true
        }
    }
}