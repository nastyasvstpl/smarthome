package com.example.smarthome.screens.home

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.smarthome.R
import com.example.smarthome.base.BaseFragment
import com.example.smarthome.databinding.FragmentHomeBinding
import com.example.smarthome.di.ViewModelProviderFactory
import com.example.smarthome.models.DeviceType
import com.example.smarthome.models.Filter
import com.example.smarthome.screens.home.adapters.DeviceAdapter
import com.example.smarthome.screens.home.adapters.FilterAdapter
import com.example.smarthome.utils.collapse
import com.example.smarthome.utils.expand
import javax.inject.Inject

class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    override val layoutId = R.layout.fragment_home

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory

    private lateinit var adapter: DeviceAdapter
    private lateinit var filterAdapter: FilterAdapter

    private lateinit var viewModel: HomeViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapters()
        viewModel = ViewModelProvider(
            requireActivity(),
            viewModelProviderFactory
        ).get(HomeViewModel::class.java)
        setupObservables()
        viewModel.getAllDevices()
    }

    private fun setupAdapters() {
        adapter = DeviceAdapter()
        filterAdapter = FilterAdapter()
        filterAdapter.addItems(generateFilters())
        binding.deviseRv.adapter = adapter
        binding.filterRv.adapter = filterAdapter
        binding.filterBtn.setOnClickListener {
            if (binding.recyclerCv.isVisible) {
                binding.recyclerCv.collapse()
                applyFilters()
            } else {
                binding.recyclerCv.expand()
            }
        }

        adapter.onItemClick = {
            val action = HomeFragmentDirections.actionHomeFragmentToDeviceDetails(adapter.items[it])
            findNavController().navigate(action)
        }

        adapter.onRemoveClick = { deviceId ->
            viewModel.saveDeletedDevice(deviceId)
        }
    }

    private fun setupObservables() {
        viewModel.deviceLiveData.removeObservers(this)
        viewModel.deviceLiveData.observe(viewLifecycleOwner) { devices ->
            adapter.addItems(devices)
        }
    }

    private fun generateFilters(): MutableList<Filter> {
        val filters = mutableListOf<Filter>()
        DeviceType.values().iterator().forEach {
            if (it != DeviceType.OTHER) filters.add(Filter(title = it.name, deviceType = it))
        }
        return filters
    }

    private fun applyFilters() {
        val selectedFilters = filterAdapter.items.filter { it.isSelected }
        if (selectedFilters.isEmpty()) {
            adapter.addItems(viewModel.deviceLiveData.value ?: return)
        } else {
            adapter.applyFilters(selectedFilters, viewModel.deviceLiveData.value)
        }
    }
}

