package com.example.smarthome.screens.main

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.navigation.NavController
import com.example.smarthome.R
import com.example.smarthome.base.BaseActivity
import com.example.smarthome.databinding.ActivityMainBinding
import javax.inject.Inject
import javax.inject.Provider

class MainActivity : BaseActivity<ActivityMainBinding>() {

    @Inject
    lateinit var navControllerProvider: Provider<NavController>

    override val layoutId: Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(binding) {
            homeBtn.setOnClickListener {
                homeBtn.isEnabled = false
                profileBtn.isEnabled = true
                profileTv.alpha = 0.7F
                homeTv.alpha = 1F
                navControllerProvider.get().navigate(R.id.homeFragment)
            }

            profileBtn.setOnClickListener {
                profileBtn.isEnabled = false
                homeBtn.isEnabled = true
                homeTv.alpha = 0.7F
                profileTv.alpha = 1F
                navControllerProvider.get().navigate(R.id.userInfoFragment)
            }
        }
    }

    fun setButtonHolderVisibility(value: Boolean) {
        binding.bottomCv.isVisible = value
    }

    fun setUpTitleVisibility(value: Boolean) {
        binding.appNane.isVisible = value
    }
}