package com.example.smarthome.screens.user

import android.app.Application
import android.content.Context
import androidx.core.content.edit
import androidx.lifecycle.MutableLiveData
import com.example.smarthome.base.BaseViewModel
import com.example.smarthome.models.User
import com.example.smarthome.network.Api
import com.example.smarthome.utils.Constants.PREFERENCES
import com.example.smarthome.utils.Constants.USER_PREFS
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class UserInfoViewModel @Inject constructor(var api: Api, application: Application) : BaseViewModel() {

    private val preferences = application.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE)

    val userLiveData = MutableLiveData<User?>()

    private fun getUserFromRemote() {
        launch(Dispatchers.IO) {
            val response = api.getUserInfo()
            userLiveData.postValue(response.user)
            saveUserToPref(response.user)
        }
    }


    private fun getUserFromPref(): User? {
        val json: String = preferences.getString(USER_PREFS, null) ?: return null
        return Gson().fromJson(json, User::class.java)
    }


     fun saveUserToPref(user: User?) {
        preferences.edit {
            putString(USER_PREFS, Gson().toJson(user))
        }
    }

    fun requireUser() {
        val user = getUserFromPref()
        if (user != null) {
            userLiveData.value = user
        } else {
            getUserFromRemote()
        }
    }
}