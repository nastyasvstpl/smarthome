package com.example.smarthome.screens.device

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.smarthome.R
import com.example.smarthome.base.BaseFragment
import com.example.smarthome.databinding.FragmentDeviceDetailsBinding
import com.example.smarthome.di.ViewModelProviderFactory
import com.example.smarthome.models.Device
import com.example.smarthome.models.DeviceType
import com.example.smarthome.models.applyImage
import com.example.smarthome.screens.main.MainActivity
import kotlinx.android.synthetic.main.fragment_device_details.*
import javax.inject.Inject


class DeviceDetailsFragment : BaseFragment<FragmentDeviceDetailsBinding>() {
    override val layoutId = R.layout.fragment_device_details
    private val args: DeviceDetailsFragmentArgs by navArgs()
    private lateinit var device: Device

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory
    private lateinit var viewModel: DeviceDetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as MainActivity).setButtonHolderVisibility(value = false)
        (activity as MainActivity).setUpTitleVisibility(value = false)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(requireActivity(), viewModelProviderFactory).get(
            DeviceDetailsViewModel::class.java
        )
        device = args.device
        initDeviceData()
        setupListeners()
        initView()
    }

    private fun setupListeners() {
        binding.backBtn.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.saveBtn.setOnClickListener {
            viewModel.saveDeviceDetailsToPref(updateDeviceDetails())
            Toast.makeText(requireContext(), getString(R.string.save_settings), Toast.LENGTH_SHORT).show()
        }

        binding.deviceSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            binding.seekBar.isEnabled = isChecked
        }

        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (device.type == DeviceType.HEATER) {
                    if (progress < MIN_HEATER_VALUE) {
                        binding.progressTv.text =
                            getString(
                                R.string.temperature,
                                (progress + MIN_HEATER_VALUE).toString()
                            )
                    } else {
                        binding.progressTv.text =
                            getString(R.string.temperature, progress.toString())
                    }
                } else {
                    binding.progressTv.text = ("$progress%")
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })
    }

    private fun initDeviceData() {
        val savedDevice = viewModel.getDeviceDetailsFromPref(device.id)
        if (savedDevice != null) device = savedDevice
        with(binding) {
            deviceNameTv.text = device.deviceName
            device.applyImage(root.context)
            if (device.image != null) deviceIv.setImageDrawable(device.image)

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity as MainActivity).setButtonHolderVisibility(value = true)
        (activity as MainActivity).setUpTitleVisibility(value = true)
    }

    private fun initView() {
        if (device.mode == null) {
            deviceSwitch.isVisible = false
        } else {
            binding.deviceSwitch.isChecked = device.isChecked
        }

        when (device.type) {
            DeviceType.LIGHT -> {
                seekBar.isEnabled = device.isChecked
                binding.seekBar.progress = device.intensity ?: 0
                binding.progressTv.setText((device.intensity?.toString()) + "%")
                binding.settingsName.setText(R.string.light_hint)
            }

            DeviceType.ROLLERSHUTTER -> {
                binding.seekBar.progress = device.position ?: 0
                binding.progressTv.setText(device.position?.toString())
                binding.settingsName.setText(R.string.roller_shutter_hint)
            }
            DeviceType.HEATER -> {
                seekBar.isEnabled = device.isChecked
                seekBar.max = MAX_HEATER_VALUE
                binding.seekBar.progress = device.temperature ?: 0
                binding.progressTv.setText((device.temperature?.toString()) + "°C")
                binding.settingsName.setText(R.string.heater_hint)
            }
        }

    }

    private fun updateDeviceDetails(): Device {
        return when (device.type) {
            DeviceType.LIGHT -> {
                device.copy(
                    intensity = binding.seekBar.progress,
                    mode = binding.deviceSwitch.toString()
                )
            }

            DeviceType.ROLLERSHUTTER -> {
                device.copy(
                    position = binding.seekBar.progress
                )
            }
            DeviceType.HEATER -> {
                device.copy(
                    temperature = binding.seekBar.progress,
                    mode = binding.deviceSwitch.toString()
                )
            }
            DeviceType.OTHER -> device
        }
    }

    companion object {
        private const val MAX_HEATER_VALUE = 28
        private const val MIN_HEATER_VALUE = 7
    }
}