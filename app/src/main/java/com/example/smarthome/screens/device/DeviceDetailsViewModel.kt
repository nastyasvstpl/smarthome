package com.example.smarthome.screens.device

import android.app.Application
import android.content.Context
import androidx.core.content.edit
import androidx.lifecycle.MutableLiveData
import com.example.smarthome.base.BaseViewModel
import com.example.smarthome.models.Device
import com.example.smarthome.network.Api
import com.example.smarthome.utils.Constants
import com.google.gson.Gson
import javax.inject.Inject

class DeviceDetailsViewModel @Inject constructor(var api: Api, application: Application) :
    BaseViewModel() {
    private val preferences = application.getSharedPreferences(Constants.PREFERENCES, Context.MODE_PRIVATE)

    val deviceDetailsLiveData = MutableLiveData<Device>()

    fun saveDeviceDetailsToPref(device: Device?) {
        device?.let {
            preferences.edit {
                putString(device.id.toString(), Gson().toJson(device))
            }
        }
    }

    fun getDeviceDetailsFromPref(deviceId: Int): Device? {
        val json: String = preferences.getString(deviceId.toString(), null) ?: return null
        return Gson().fromJson(json, Device::class.java)
    }



}