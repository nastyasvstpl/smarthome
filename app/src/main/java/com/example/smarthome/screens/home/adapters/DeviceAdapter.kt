package com.example.smarthome.screens.home.adapters

import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import com.example.smarthome.R
import com.example.smarthome.base.BaseBindingAdapter
import com.example.smarthome.databinding.DeviceItemBinding
import com.example.smarthome.models.Device
import com.example.smarthome.models.Filter
import com.example.smarthome.models.applyImage
import com.example.smarthome.utils.Constants.FADE_DURATION

class DeviceAdapter : BaseBindingAdapter<Device, DeviceAdapter.ViewHolder, DeviceItemBinding>() {

    var onRemoveClick: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(initBinding(R.layout.device_item, parent))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        if (isInPositionsRange(position)) {
            holder.binding.deleteBtn.setOnClickListener {
                val device = items.removeAt(holder.bindingAdapterPosition)
                notifyItemRemoved(holder.bindingAdapterPosition)
                onRemoveClick?.invoke(device.id)
            }
            holder.onBind(items[holder.adapterPosition], holder.adapterPosition)
            setScaleAnimation(holder.itemView)
        }
    }

    class ViewHolder(binding: DeviceItemBinding) :
        BaseBindingViewHolder<Device, DeviceItemBinding>(binding) {
        override fun onBind(item: Device, position: Int) {
            binding.diviceName.text = item.deviceName
            binding.diviceName.isSelected = true
            item.applyImage(binding.root.context)
            if (item.image != null) binding.deviceIcon.setImageDrawable(item.image)
        }
    }

    private fun setScaleAnimation(view: View) {
        val anim = ScaleAnimation(
            0.7f,
            1.0f,
            0.7f,
            1.0f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        anim.duration = FADE_DURATION.toLong()
        view.startAnimation(anim)
    }

    fun applyFilters(filters: List<Filter>, allDevices: List<Device>?) {
        if (allDevices.isNullOrEmpty()) return
        val list = allDevices.filter { device -> filters.any { filter -> filter.deviceType == device.type } }
        super.addItems(list)
    }
}

