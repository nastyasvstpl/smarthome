package com.example.smarthome.screens.home

import android.app.Application
import android.content.Context
import androidx.core.content.edit
import androidx.lifecycle.MutableLiveData
import com.example.smarthome.base.BaseViewModel
import com.example.smarthome.models.Device
import com.example.smarthome.network.Api
import com.example.smarthome.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(var api: Api, application: Application) : BaseViewModel() {

    private val preferences =
        application.getSharedPreferences(Constants.PREFERENCES, Context.MODE_PRIVATE)

    val deviceLiveData = MutableLiveData<List<Device>>()

    fun getAllDevices() {
        launch(Dispatchers.IO) {
            val response = api.getAllDevices()
            val devices = filterDeletedDevices(response.devices ?: mutableListOf())
            deviceLiveData.postValue(devices)
        }
    }

    private fun saveDeletedDevicesToPref(devices: MutableSet<String>) {
        preferences.edit {
            putStringSet(Constants.DELETED_DEVICES, devices)
        }
    }

    private fun getDeletedDevicesFromPref(): MutableSet<String> {
        return preferences.getStringSet(Constants.DELETED_DEVICES, mutableSetOf()) ?: mutableSetOf()
    }

    fun saveDeletedDevice(deviceId: Int) {
        val set = getDeletedDevicesFromPref()
        set.add(deviceId.toString())
        saveDeletedDevicesToPref(set)
    }

    private fun filterDeletedDevices(devices: List<Device>): List<Device> {
        val deletedDevices = getDeletedDevicesFromPref()
        return if (deletedDevices.isNotEmpty()) {
            devices.filter { !deletedDevices.contains(it.id.toString()) }
        } else {
            devices
        }
    }
}

