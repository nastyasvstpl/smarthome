package com.example.smarthome.screens.splash

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import com.example.smarthome.R
import com.example.smarthome.base.BaseActivity
import com.example.smarthome.databinding.ActivitySplashBinding
import com.example.smarthome.screens.main.MainActivity

class SplashActivity : BaseActivity<ActivitySplashBinding>() {

    override val layoutId: Int = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        object : CountDownTimer(TIMER_DELAY, TIMER_INTERVAL) {
            override fun onTick(millisUntilFinished: Long) {
                val finishedSeconds = TIMER_DELAY - millisUntilFinished
                val total = (finishedSeconds.toFloat() / TIMER_DELAY.toFloat() * 100.0).toInt()

            }

            override fun onFinish() {
                startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                finish()
            }
        }.start()
    }

    companion object {
        private const val TIMER_DELAY = 3000L
        private const val TIMER_INTERVAL = 10L
    }
}