package com.example.smarthome.network

import com.example.smarthome.models.DataResponse
import com.example.smarthome.models.Device
import com.example.smarthome.models.User
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {

    @GET("modulotest/data.json")
    suspend fun getAllDevices(): DataResponse<List<Device>>

    @GET("modulotest/data.json")
    suspend fun getDevice(@Path("id") id: Int): DataResponse<Device>

    @GET("modulotest/data.json")
    suspend fun getUserInfo(): DataResponse<User>

}