package com.example.smarthome.utils

object Constants {
    const val BASE_URL: String = "http://storage42.com/"
    const val FADE_DURATION = 500
    const val PREFERENCES = "preferences"
    const val USER_PREFS = "USER_PREFS"
    const val DELETED_DEVICES = "DELETED_DEVICES"
}