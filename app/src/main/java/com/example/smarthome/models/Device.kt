package com.example.smarthome.models


import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.res.ResourcesCompat
import com.example.smarthome.R
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Device(
    @SerializedName("deviceName")
    val deviceName: String?,
    @SerializedName("id")
    val id: Int,
    @SerializedName("intensity")
    val intensity: Int?,
    @SerializedName("mode")
    val mode: String?,
    @SerializedName("position")
    val position: Int?,
    @SerializedName("productType")
    val productType: String?,
    @SerializedName("temperature")
    val temperature: Int?
) : Serializable {
    val type: DeviceType get() = DeviceType.valueOf(productType?.uppercase() ?: "OTHER")
    val isChecked: Boolean
        get() {
            return if (mode?.lowercase() == "on") true else false
        }

    var image: Drawable? = null
}

fun Device.applyImage(context: Context) {
    this.image = when (this.type) {
        DeviceType.LIGHT -> ResourcesCompat.getDrawable(
            context.resources,
            R.drawable.ic_light,
            null
        )
        DeviceType.ROLLERSHUTTER -> ResourcesCompat.getDrawable(
            context.resources,
            R.drawable.ic_jalousie,
            null
        )
        DeviceType.HEATER -> ResourcesCompat.getDrawable(
            context.resources,
            R.drawable.ic_heater,
            null
        )
        DeviceType.OTHER -> null
    }
}