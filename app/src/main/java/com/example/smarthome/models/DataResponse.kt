package com.example.smarthome.models


import com.google.gson.annotations.SerializedName

data class DataResponse<T>(
    @SerializedName("devices")
    val devices: List<Device>?,
    @SerializedName("user")
    val user: User?
)