package com.example.smarthome.models


import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("address")
    val address: Address?,
    @SerializedName("birthDate")
    val birthDate: Long?,
    @SerializedName("firstName")
    val firstName: String?,
    @SerializedName("lastName")
    val lastName: String?
)