package com.example.smarthome.models


data class Filter(
    val title: String,
    var isSelected: Boolean = false,
    val deviceType: DeviceType
)