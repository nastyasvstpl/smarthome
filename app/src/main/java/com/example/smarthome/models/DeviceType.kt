package com.example.smarthome.models

enum class DeviceType {
    HEATER, LIGHT, ROLLERSHUTTER, OTHER
}