package com.example.smarthome.models


import com.google.gson.annotations.SerializedName

data class Address(
    @SerializedName("city")
    val city: String?,
    @SerializedName("country")
    val country: String?,
    @SerializedName("postalCode")
    val postalCode: Int?,
    @SerializedName("street")
    val street: String?,
    @SerializedName("streetCode")
    val streetCode: String?
)